package fr.scholanova.eial.archidist.PresentationEjb;

import javax.ejb.Remote;
import javax.ejb.Singleton;

import fr.scholanova.eial.archidist.PresentationEjbInterface.CalculInterface;

@Remote(CalculInterface.class)
@Singleton
public class Calcul implements CalculInterface {
	
	public int add(int i, int j) {
		return i + j;
	}
	
	public double power(double i, double power) {
		return Math.pow(i, power);
	}

}
