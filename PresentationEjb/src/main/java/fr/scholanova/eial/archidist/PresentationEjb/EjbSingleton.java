package fr.scholanova.eial.archidist.PresentationEjb;

import javax.ejb.Remote;
import javax.ejb.Singleton;

import fr.scholanova.eial.archidist.PresentationEjbInterface.EjbTestEtat;

@Remote(EjbTestEtat.class)
@Singleton
public class EjbSingleton implements EjbTestEtat {

	private int nb;
	
	@Override
	public void increment() {
		nb++;
	}

	@Override
	public int getNb() {
		return this.nb;
	}

}
