package fr.scholanova.eial.archidist.PresentationEjb;

import javax.ejb.Stateful;
import javax.ejb.Remote;

import fr.scholanova.eial.archidist.PresentationEjbInterface.EjbTestEtat;


@Remote(EjbTestEtat.class)
@Stateful
public class EjbStateful implements EjbTestEtat {
	private int nb;
	
	@Override
	public void increment() {
		nb++;
	}
	
	@Override
	public int getNb() {
		return this.nb;
	}

}
